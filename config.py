# import the necessary packages
from os import path


# flask ip
SERVER_URL = "http://127.0.0.1:5000/"

# flask port
SERVER_PORT = 5000

# upload folders
IMAGE_UPLOADS = "uploads/"
TEMP_IMAGES = "uploads/Temp"


TESSRACT_PATH = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


# HEROKU_TESRACT_PATH = "/app/.apt/usr/bin/tesseract"